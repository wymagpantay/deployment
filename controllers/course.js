const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.addCourse = (req,res) =>{
	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	}) 
	// saves the created object to our database
	return newCourse.save().then((course,error)=>{
		if(error){
			return false
		} else {
			return res.send(true)
		}
	})
	.catch(err=>err)
}


// Retrieve all courses
/*
	1. Retrieve all the courses from the database
*/
// We will use the find() method for our course model.
module.exports.getAllCourses = (req,res) =>{
	return Course.find({}).then(result=>{
		return res.send(result)
	})
}

// getAllActiveCourses
// crete a function that will handle req,res

module.exports.getAllActiveCourses = (req,res) =>{
	return Course.find({isActive:true}).then(result=>{
		return res.send(result)
	})
}

// Get a specific course
module.exports.getCourse = (req,res) =>{
	return Course.findById(req.params.courseId).then(result=>{
		return res.send(result)
	})
}

// Get a specific course
module.exports.getCourse = (req,res) =>{
	return Course.findById(req.params.courseId).then(result=>{
		return res.send(result)
	})
}

// Get a specific course
module.exports.updateCourse = (req,res) =>{
	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId,updatedCourse).then((course,err)=>{
		if(err){
			return res.send(false)
		} else {
			return res.send (true)
		}
	})
}

// Archive a course
module.exports.archiveCourse = (req, res) => {
    Course.findById(req.params.courseId).then(course => {
        if (course.isActive === true) {
            course.isActive = false;
            return course.save().then(updatedCourse => {
                if (updatedCourse) {
                    return res.send(true);
                } else {
                    return res.send(false);
                }
            });
        }
    }).catch(err => {
        console.error(err);
        return res.send(false);
    });
};


// Activate a course
module.exports.activateCourse = (req, res) => {
    Course.findById(req.params.courseId).then(course => {
        if (course.isActive === false) {
            course.isActive = true;
            return course.save().then(updatedCourse => {
                if (updatedCourse) {
                    return res.send(true);
                } else {
                    return res.send(false);
                }
            });
        }
    }).catch(err => {
        console.error(err);
        return res.send(false);
    });
};


